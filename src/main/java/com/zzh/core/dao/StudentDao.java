package com.zzh.core.dao;

import com.zzh.core.base.BaseDao;
import com.zzh.core.model.Student;

public interface StudentDao extends BaseDao<Student, Integer> {

}