package com.zzh.core.service.impl;

import com.zzh.core.dao.StudentDao;
import com.zzh.core.model.Student;
import com.zzh.core.service.StudentService;
import javax.annotation.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class StudentServiceImpl implements StudentService {

  @Resource
  private StudentDao studentDao;

  @Override
  public Student save(Student student) {
    return studentDao.save(student);
  }

  @Override
  public int update(Student student) {
    return studentDao.update(student);
  }

  @Override
  public Student findOne(Integer id) {
    return studentDao.findOne(id);
  }

  @Override
  public void delete(Integer id) {
    studentDao.delete(id);
  }

  @Override
  public Page<Student> page(Pageable pageable, Student student) {
    return studentDao.page(pageable, student);
  }

}