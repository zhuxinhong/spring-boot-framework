package com.zzh.core.service;

import com.zzh.core.base.BaseService;
import com.zzh.core.model.Student;

public interface StudentService extends BaseService<Student, Integer> {

}