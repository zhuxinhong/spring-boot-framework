package com.zzh.core.model;

import java.io.Serializable;

public class Student implements Serializable {

  private Integer id;

  private String name;

  private Integer phone;

  private java.sql.Timestamp ctime;

  private java.sql.Timestamp mtime;

  private Boolean deleted;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer value) {
    this.id = value;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String value) {
    this.name = value;
  }

  public Integer getPhone() {
    return this.phone;
  }

  public void setPhone(Integer value) {
    this.phone = value;
  }

  public java.sql.Timestamp getCtime() {
    return this.ctime;
  }

  public void setCtime(java.sql.Timestamp value) {
    this.ctime = value;
  }

  public java.sql.Timestamp getMtime() {
    return this.mtime;
  }

  public void setMtime(java.sql.Timestamp value) {
    this.mtime = value;
  }

  public Boolean getDeleted() {
    return this.deleted;
  }

  public void setDeleted(Boolean value) {
    this.deleted = value;
  }

}

