package com.zzh.core.factory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by zhuxh on 16/7/27.
 */
@Component
public class GsonFactory {

  @Bean
  public Gson gson() {
    Gson gson = new GsonBuilder()
        .enableComplexMapKeySerialization() //支持Map的key为复杂对象的形式
        .serializeNulls().setDateFormat("yyyy-MM-dd HH:mm:ss")
        .setVersion(1.0)
        .create();
    return gson;
  }

}
