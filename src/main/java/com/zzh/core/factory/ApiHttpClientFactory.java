package com.zzh.core.factory;

import com.google.gson.Gson;
import com.zzh.core.config.HttpConnConfig;
import common.httpclient.AppApiHttpClient;
import common.httpclient.HttpApiClient;
import javax.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ApiHttpClientFactory {

  @Resource
  private Gson gson;

  @Resource
  private HttpConnConfig httpConnConfig;

  @Bean
  public AppApiHttpClient xxHttpClient() {
    HttpApiClient httpApiClient = new HttpApiClient(httpConnConfig.getConnectTimeout(),
        httpConnConfig.getSocketTimeout()).init();
    return new AppApiHttpClient(httpApiClient, gson);
  }


}
