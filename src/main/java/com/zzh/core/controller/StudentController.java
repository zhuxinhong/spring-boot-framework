package com.zzh.core.controller;

import com.zzh.core.model.Student;
import com.zzh.core.service.StudentService;
import javax.annotation.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student")
public class StudentController {

  @Resource
  private StudentService studentService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Student create(@RequestBody Student student) {
    return studentService.save(student);
  }

  @GetMapping("{id}")
  public Student student(@PathVariable Integer id) {
    return studentService.findOne(id);
  }

  @PutMapping
  public void update(Student student) {
    studentService.update(student);
  }

  @DeleteMapping("{id}")
  public void delete(@PathVariable Integer id) {
    studentService.delete(id);
  }

  @GetMapping
  public Page<Student> page(@PageableDefault(page = 1) Pageable pageable, Student student) {
    return studentService.page(pageable, student);
  }

}