package com.zzh.core.controller;

import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by zhuxh on 16/5/11.
 */
@RestController
public class IndexController {

  @Resource
  private RedisTemplate redisTemplate;

  @GetMapping("/")
  public String index() {
    return "success";
  }

  @GetMapping("redis/test")
  public void redisTest() {
    Long ts = System.currentTimeMillis();
    System.out.println("currentTs: " + ts);
    redisTemplate.opsForValue().set("test", ts.toString(), 10, TimeUnit.SECONDS);
    String val = redisTemplate.opsForValue().get("test").toString();
    System.out.println("redisValue: " + val);
  }

  @GetMapping("menu")
  public ModelAndView menu() {
    return new ModelAndView("menu");
  }

  @GetMapping(value = {"index"})
  public ModelAndView hello() {
    return new ModelAndView("index", "messages", "Hello world");
  }

}
