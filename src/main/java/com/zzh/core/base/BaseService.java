package com.zzh.core.base;

import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by zhuxh on 16/11/30.
 */
public interface BaseService<T, ID extends Serializable> {

  T save(T t);

  int update(T t);

  T findOne(ID id);

  void delete(ID id);

  Page<T> page(Pageable pageable, T t);

}
