package com.zzh.core.base;

import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by zhuxh on 17/5/31.
 */
public interface BaseDao<T, ID extends Serializable> {

  T save(T t);

  int update(T t);

  T findOne(ID id);

  void delete(ID id);

  Page<T> page(Pageable pageable, T t);
}
