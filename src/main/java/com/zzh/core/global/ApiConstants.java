package com.zzh.core.global;

public interface ApiConstants {

  /**
   * 通用字段
   **/
  int CODE_REQ_ERROR = -400;

  int CODE_SERVER_ERROR = -500;

  int CODE_API_PARAM_ERROR = -701;

  String CODE_KEY = "code";
  String TS_KEY = "ts";
  String MSG_KEY = "msg";
  String DATA_KEY = "data";
  String REQUEST_KEY = "request-id";

}
