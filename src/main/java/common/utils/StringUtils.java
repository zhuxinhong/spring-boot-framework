package common.utils;


import java.util.Map;
import java.util.UUID;

/**
 * Created by zhuxh on 16/5/10.
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {

  public static String getUUID() {
    return UUID.randomUUID().toString().replace("-", "");
  }

  public static Boolean isExistNullOrEmpty(String... texts) {
    if (texts == null) {
      return true;
    } else {
      for (String obj : texts) {
        if (obj == null) {
          return true;
        }
        if (StringUtils.isBlank(obj)) {
          return true;
        }
      }
    }
    return false;
  }

  public static String parseObjargs(Object[] args) {
    if (args == null || args.length == 0) {
      return "";
    }

    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < args.length; i++) {
      if (args[i] == null) {
        continue;
      }

      sb.append(args[i].toString());
      if (i != args.length - 1) {
        sb.append(",");
      }
    }

    return sb.toString();
  }

  public static String parseUrlArgs(Map<String, String> params) {
    String str = "";
    for (Map.Entry<String, String> entry : params.entrySet()) {
      str = String.format("%s%s=%s&", str, entry.getKey(), entry.getValue(), "&");
    }

    if (str.length() > 0) {
      str = str.substring(0, str.length() - 1);
    }

    return str;
  }

  public static int defaultInteger(String str, Integer defaultInt) {
    if (isEmpty(str) && defaultInt == null) {
      defaultInt = 0;
    }

    int iRet;
    String ret = isEmpty(str) ? defaultInt + "" : str;
    try {
      iRet = Integer.parseInt(ret);
    } catch (Exception e) {
      return defaultInt;
    }
    return iRet;
  }
}
