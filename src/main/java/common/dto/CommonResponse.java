package common.dto;

import java.io.Serializable;

public class CommonResponse<T> implements Serializable {

  public CommonResponse() {
    this.ts = System.currentTimeMillis();
  }

  public CommonResponse(Integer code, String msg) {
    this();
    this.code = code;
    this.msg = msg;
  }

  private Integer code;

  private Long ts;

  private String msg;

  private T data;

  private String requestId;

  private Long cost;

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public Long getTs() {
    return ts;
  }

  public void setTs(Long ts) {
    this.ts = ts;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public Long getCost() {
    return cost;
  }

  public void setCost(Long cost) {
    this.cost = cost;
  }

  @Override
  public String toString() {
    return "CommonResponse{" +
        "code=" + code +
        ", ts=" + ts +
        ", msg='" + msg + '\'' +
        ", data=" + data +
        ", requestId='" + requestId + '\'' +
        ", cost=" + cost +
        '}';
  }
}
